package com.example.uapv1702032.tp1_mob;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CountryList country = new CountryList();

        final ListView listview = (ListView) findViewById(R.id.countList);

        final String[] values = country.getNameArray();

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, values);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
// Do something in response to the click
                final String item = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                String name = listview.getItemAtPosition(position).toString();
                intent.putExtra("name",name);
                startActivity(intent);

            }
        });
    }
}

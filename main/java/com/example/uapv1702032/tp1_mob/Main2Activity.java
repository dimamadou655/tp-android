package com.example.uapv1702032.tp1_mob;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {


    TextView countryName;
    EditText inputCapitale;
    EditText inputLangue;
    EditText inputMonnaie;
    EditText inputPopulation;
    EditText inputSuperficie;
    Country country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        String name = getIntent().getStringExtra("name");
        country = CountryList.getCountry(name);

        ImageView image = findViewById(R.id.flag);
        int id = getResources().getIdentifier(country.getmImgFile(),"drawable",getPackageName());
        image.setImageResource(id);

        countryName = (TextView) findViewById(R.id.textName);
        countryName.setText(name);

        inputCapitale = findViewById(R.id.inputCapitale);
        inputCapitale.setText(country.getmCapital());

        inputLangue = findViewById(R.id.inputLangue);
        inputLangue.setText(country.getmLanguage());

        inputMonnaie = findViewById(R.id.inputMonnaie);
        inputMonnaie.setText(country.getmCurrency());

        inputPopulation = findViewById(R.id.inputPopulation);
        inputPopulation.setText(country.getmPopulation()+"");

        inputSuperficie = findViewById(R.id.inputSuperficie);
        inputSuperficie.setText(country.getmArea()+"");

        Button save = findViewById(R.id.button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                country.setmCapital(inputCapitale.getText().toString());
                country.setmLanguage(inputLangue.getText().toString());
                country.setmCurrency(inputMonnaie.getText().toString());
                country.setmPopulation(Integer.parseInt(inputPopulation.getText().toString()));
                country.setmArea(Integer.parseInt(inputSuperficie.getText().toString()));
                Toast.makeText(getApplication(),"Sauvegarde effectué",Toast.LENGTH_LONG).show();
            }
        });


    }
}
